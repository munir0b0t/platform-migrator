===========
Future work
===========

* Currently, platform migrator stores history of all migrations done, including
  failed attempts. However, the user cannot easily see this and the history is
  not used for anything. Work can be done around how this history can be used
  to determine systems and packages that are not compatible with each other.
  Also, features can be built around querying it.

* More package managers can be added and configured to be available by default.
  Also, there can be an option to list the usable package managers available on
  the system.

* Add integration for fetching software from Github, Gitlab and other git
  providers. Right now, the software needs to be fetched manually using
  ``git clone`` and then the conda environment needs to be setup using the
  ``meta.yaml``. This can be automated in future iterations so that only the
  git repo URL is required.
