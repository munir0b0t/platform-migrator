"""Test for conda to conda migration of scikit application and no vm"""


import os
import shutil
import subprocess
import sys
import time
import unittest


HOST = 'localhost'
PORT = 9001
HOME = os.getenv('HOME')
CONDA_ROOT_DIR = os.getenv('CONDA_ROOT_DIR') + '/bin'


class Test_simple_app(unittest.TestCase):

    def setUp(self):
        os.chdir(os.path.dirname(os.path.abspath(__file__)))
        self.work_dir = os.getcwd()
        proc0 = subprocess.run(
            '%s/conda create -y -n scikit-app-orig scikit-learn scikit-image python=2.7' %
            CONDA_ROOT_DIR if CONDA_ROOT_DIR else os.getcwd(),
            stdout=subprocess.PIPE, stderr=subprocess.STDOUT, shell=True)
        if proc0.returncode != 0:
            self.fail('Failed to create test environment for conda')
        subprocess.run([sys.executable, '../../platform_migrator/main.py', 'server',
                        '--host', HOST, '--port', str(PORT), 'start'])
        time.sleep(1)

    def tearDown(self):
        os.chdir(self.work_dir)
        if os.path.isdir('/tmp/scikit-app'):
            shutil.rmtree('/tmp/scikit-app')
        if os.path.exists('base_sys_script.py'):
            os.remove('base_sys_script.py')
        if os.path.isdir(HOME + '/.platform_migrator/scikit-app'):
            shutil.rmtree(HOME + '/.platform_migrator/scikit-app')
        subprocess.run([sys.executable, '../../platform_migrator/main.py', 'server', 'stop'])
        subprocess.run('%s/conda env remove -y -n scikit-app' %
                       CONDA_ROOT_DIR if CONDA_ROOT_DIR else os.getcwd(),
                       stdout=subprocess.PIPE, stderr=subprocess.STDOUT, shell=True)
        subprocess.run('%s/conda env remove -y -n scikit-app-orig' %
                       CONDA_ROOT_DIR if CONDA_ROOT_DIR else os.getcwd(),
                       stdout=subprocess.PIPE, stderr=subprocess.STDOUT, shell=True)

    def test_migration(self):
        proc1 = subprocess.run('curl -s http://%s:%d/migrate' % (HOST, PORT), shell=True,
                               stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
        self.assertEqual(proc1.returncode, 0, proc1.stdout.decode('utf-8'))
        with open('base_sys_script.py', 'w+') as f_obj:
            f_obj.write(proc1.stdout.decode('utf-8'))

        os.chdir(CONDA_ROOT_DIR if CONDA_ROOT_DIR else self.work_dir)
        proc2 = subprocess.run('python %s/base_sys_script.py' % self.work_dir, shell=True,
                               input=(b'scikit-app-orig\nscikit-app\n%s/scikit-app'
                                      % self.work_dir.encode('utf-8')),
                               stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
        self.assertEqual(proc2.returncode, 0, proc2.stdout.decode('utf-8'))
        os.chdir(self.work_dir)

        self.assertTrue(os.path.isdir(HOME + '/.platform_migrator/scikit-app'))
        proc3 = subprocess.run([sys.executable, '../../platform_migrator/main.py', 'migrate',
                                'scikit-app', 'test-config.ini'],
                               stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
        self.assertEqual(proc3.returncode, 0, proc3.stdout.decode('utf-8'))
        self.assertTrue(os.path.isdir('/tmp/scikit-app'), proc3.stdout.decode('utf-8'))



if __name__ == '__main__':
    unittest.main()
