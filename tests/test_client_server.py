"""Tests for the client-server mechanism"""

import base64
import json
import os
import shutil
import signal
import subprocess
import sys
import time
import urllib.request
import unittest

os.chdir(os.path.dirname(os.path.abspath(__file__)) + '/../')
sys.path.insert(0, os.getcwd())
os.chdir('tests')


HOST='localhost'
PORT=9001


class Test_client_server(unittest.TestCase):

    def setUp(self):
        _data = base64.urlsafe_b64encode(b'Test data\n').decode('utf-8')
        self.data = json.dumps({'name': 'test-app',
                                'data': _data}).encode('utf-8')
        self.proc = subprocess.Popen([sys.executable, '../platform_migrator/server.py',
                                      HOST, str(PORT), os.getcwd()])
        time.sleep(1)

    def tearDown(self):
        os.kill(self.proc.pid, signal.SIGTERM)
        self.proc.wait()
        if os.path.isdir('test-app'):
            shutil.rmtree('test-app')

    def test_get(self):
        req = urllib.request.Request('http://%s:%d/migrate' % (HOST, PORT),
                                     method='GET')
        with urllib.request.urlopen(req) as resp:
            contents = resp.read().decode('utf-8')
        with open('base_sys_script.py') as exp:
            exp_contents = exp.read()
        self.assertEqual(resp.getcode(), 200)
        self.assertEqual(contents, exp_contents)

    def test_post_yml(self):
        req = urllib.request.Request('http://%s:%d/yml' % (HOST, PORT),
                                     data=self.data)
        resp = urllib.request.urlopen(req)
        self.assertEqual(resp.getcode(), 200)
        self.assertTrue(os.path.isdir('test-app'))
        self.assertTrue(os.path.exists('test-app/env.yml'))

    def test_post_zip(self):
        req = urllib.request.Request('http://%s:%d/zip' % (HOST, PORT),
                                     data=self.data)
        resp = urllib.request.urlopen(req)
        self.assertEqual(resp.getcode(), 200)
        self.assertTrue(os.path.isdir('test-app'))
        self.assertTrue(os.path.exists('test-app/pkg.zip'))

    def test_post_min(self):
        req = urllib.request.Request('http://%s:%d/min' % (HOST, PORT),
                                     data=self.data)
        resp = urllib.request.urlopen(req)
        self.assertEqual(resp.getcode(), 200)
        self.assertTrue(os.path.isdir('test-app'))
        self.assertTrue(os.path.exists('test-app/min_deps.json'))


if __name__ == '__main__':
    unittest.main()
